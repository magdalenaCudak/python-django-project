from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils import timezone


# Create your models here.

class MyUser(AbstractUser):
    first_name = models.CharField(max_length=50)
    middle_name = models.CharField(max_length=50, default='')
    last_name = models.CharField(max_length=50)
    is_student = models.BooleanField(default=True)

    REQUIRED_FIELDS = ['is_student']

    def full_name(self):
        if self.middle_name != '':
            return str(self.first_name) + ' ' + str(self.middle_name) + ' ' + str(self.last_name)
        else:
            return str(self.first_name) + ' ' + str(self.last_name)

    def __str__(self):
        return self.full_name()


class Teacher(models.Model):
    user = models.OneToOneField(MyUser, on_delete=models.CASCADE, primary_key=True, related_name='teachers_data')
    subject = models.CharField(max_length=50)

    def __str__(self):
        return str(self.user)


class HourGroup(models.Model):
    teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE, related_name='teacher')
    WEEK_DAY_CHOICE = (
        ('MON', 'MONDAY'),
        ('TUE', 'TUESDAY'),
        ('WED', 'WEDNESDAY'),
        ('THU', 'THURSDAY')
    )
    YEAR_IN_SCHOOL_CHOICES = (
        ('VII_SP', 'VII SP'),
        ('VI_SP', 'VI SP'),
        ('VIII_SP', 'VIII SP'),
        ('III_GIM', 'III GIM')
    )
    student_limit = models.IntegerField(default='10')
    participants_amount = models.IntegerField(default=0)
    class_level = models.CharField(choices=YEAR_IN_SCHOOL_CHOICES, max_length=20, default=None)
    group_name = models.CharField(max_length=50, default='', unique=True, help_text='Must be unique')
    hour = models.TimeField(default=None, help_text="Hour format is HH:MM")
    week_day = models.CharField(choices=WEEK_DAY_CHOICE, max_length=11, default=None)
    room = models.CharField(max_length=10, default=" ")

    def __str__(self):
        return self.group_name


class Student(models.Model):
    user = models.OneToOneField(MyUser, on_delete=models.CASCADE, primary_key=True, related_name='student_data')
    YEAR_IN_SCHOOL_CHOICES = (
        ('I_SP', 'I SP'),
        ('II_SP', 'II SP'),
        ('III_SP', 'III SP'),
        ('IV_SP', 'IV SP'),
        ('V_SP', 'V SP'),
        ('VI_SP', 'VI SP'),
        ('VII_SP', 'VII SP'),
        ('VIII_SP', 'VIII SP'),
        ('III_GIM', 'III GIM')
    )
    my_class = models.CharField(choices=YEAR_IN_SCHOOL_CHOICES, max_length=20)
    my_course_groups = models.ManyToManyField(HourGroup, related_name='students_group',
                                              blank=True)

    def __str__(self):
        return str(self.user)


class Communicate(models.Model):
    communicate_title = models.CharField(max_length=50)
    communicate_text = models.TextField(default="")
    pub_date = models.DateTimeField("date published", default=timezone.now)
    author = models.ForeignKey(Teacher, on_delete=models.CASCADE, related_name='author')
    destination_group = models.ForeignKey(HourGroup, on_delete=models.CASCADE)

    def __str__(self):
        return self.communicate_title
