from django.contrib.auth.forms import UserCreationForm
from django.db import transaction
from django import forms

from .models import MyUser, Student, HourGroup, Teacher, Communicate
from django.contrib.auth.models import Group


class NewGroupForm(forms.ModelForm):
    class Meta:
        model = HourGroup
        fields = ['class_level', 'group_name', 'hour', 'week_day', 'student_limit', 'room']


class NewCommunicateForm(forms.ModelForm):
    destination_group = forms.ModelChoiceField(queryset=HourGroup.objects.all(), to_field_name='group_name')

    class Meta:
        model = Communicate
        fields = ['destination_group', 'communicate_title', 'communicate_text']


class ParticipantForm(forms.Form):
    student_name = forms.ModelChoiceField(queryset=MyUser.objects.filter(is_student=True), to_field_name='username')
    group_name = forms.ModelChoiceField(queryset=HourGroup.objects.all(), to_field_name='group_name')

    class Meta:
        model = HourGroup
        fields = ['student_name', 'group_name']


class StudentRegisterForm(UserCreationForm):
    YEAR_IN_SCHOOL_CHOICES = (
        ('I_SP', 'I SP'),
        ('II_SP', 'II SP'),
        ('III_SP', 'III SP'),
        ('IV_SP', 'IV SP'),
        ('V_SP', 'V SP'),
        ('VI_SP', 'VI SP'),
        ('VII_SP', 'VII SP'),
        ('VIII_SP', 'VIII SP'),
        ('III_GIM', 'III GIM')
    )
    student_class = forms.TypedChoiceField(choices=YEAR_IN_SCHOOL_CHOICES)
    first_name = forms.CharField(max_length=50)
    middle_name = forms.CharField(max_length=50, required=False, help_text='Not required')
    last_name = forms.CharField(max_length=50)

    class Meta(UserCreationForm.Meta):
        model = MyUser

    @transaction.atomic
    def save(self, **kwargs):
        user = super().save(commit=False)
        user.is_student = True
        user.first_name = self.cleaned_data.get('first_name')
        user.middle_name = self.cleaned_data.get('middle_name')
        user.last_name = self.cleaned_data.get('last_name')
        user.save()
        student = Student.objects.create(user=user)
        student.my_class = self.cleaned_data.get('student_class')
        student.save()
        student_group = Group.objects.get(name='students')
        student_group.user_set.add(user)
        student_group.save()
        return user


class TeacherRegisterForm(UserCreationForm):
    first_name = forms.CharField(max_length=50)
    middle_name = forms.CharField(max_length=50, required=False, help_text='Not required')
    last_name = forms.CharField(max_length=50)
    subject = forms.CharField(max_length=50, required=False, help_text='Not required')

    class Meta(UserCreationForm.Meta):
        model = MyUser

    @transaction.atomic
    def save(self, *args, **kwargs):
        user = super().save(commit=False)
        user.is_student = False
        user.first_name = self.cleaned_data.get('first_name')
        user.middle_name = self.cleaned_data.get('middle_name')
        user.last_name = self.cleaned_data.get('last_name')
        user.save()
        teacher = Teacher.objects.create(user=user)
        teacher.subject = self.cleaned_data.get('subject')
        teacher.save()
        # teacher.my_group = 'teachers'
        teachers_group = Group.objects.get(name='teachers')
        teachers_group.user_set.add(user)
        teachers_group.save()
        return user
