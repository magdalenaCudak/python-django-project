from django.contrib import admin
# Register your models here.

from django.contrib.auth.admin import UserAdmin, Group, GroupAdmin
from .models import MyUser, HourGroup, Student, Teacher, Communicate

admin.site.unregister(Group)


class UserInLine(admin.TabularInline):
    model = Group.user_set.through
    extra = 0


@admin.register(Group)
class GenericGroup(GroupAdmin):
    inlines = [UserInLine]


class MyUserAdmin(admin.ModelAdmin):
    class Metal:
        model = MyUser


admin.site.register(MyUser, MyUserAdmin)
admin.site.register(HourGroup)
admin.site.register(Student)
admin.site.register(Teacher)
admin.site.register(Communicate)
