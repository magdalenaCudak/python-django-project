# Generated by Django 2.0.5 on 2018-06-03 21:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('timetable', '0013_auto_20180603_1932'),
    ]

    operations = [
        migrations.AddField(
            model_name='hourgroup',
            name='student_limit',
            field=models.IntegerField(default='10'),
        ),
    ]
