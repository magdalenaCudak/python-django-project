# Generated by Django 2.0.5 on 2018-06-05 17:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('timetable', '0017_remove_myuser_regis_date'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='student',
            name='my_course_group',
        ),
        migrations.AddField(
            model_name='student',
            name='my_course_groups',
            field=models.ManyToManyField(blank=True, null=True, related_name='students_group', to='timetable.HourGroup'),
        ),
    ]
