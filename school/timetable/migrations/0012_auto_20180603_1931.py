# Generated by Django 2.0.5 on 2018-06-03 17:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('timetable', '0011_auto_20180602_2350'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hourgroup',
            name='group_name',
            field=models.CharField(default='Unnamed', max_length=50),
        ),
    ]
