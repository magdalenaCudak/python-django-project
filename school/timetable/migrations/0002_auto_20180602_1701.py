# Generated by Django 2.0.5 on 2018-06-02 15:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('timetable', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='student',
            name='my_group',
        ),
        migrations.AlterField(
            model_name='student',
            name='my_class',
            field=models.CharField(choices=[('VII_SP', 'VII SP'), ('VI_SP', 'VI SP'), ('VIII_SP', 'VIII SP'), ('III_GIM', 'III GIM')], max_length=20),
        ),
    ]
