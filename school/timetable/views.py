from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.views.generic import CreateView
from django.contrib.auth import login, authenticate
from django.contrib.auth.models import Group
from django.contrib import messages
from datetime import timedelta, datetime
from django.contrib.auth import logout
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.decorators import login_required

from .models import MyUser, HourGroup, Teacher, Student
from .forms import StudentRegisterForm, TeacherRegisterForm, NewGroupForm, ParticipantForm, NewCommunicateForm


def is_teacher(user):
    return Group.objects.get(name='teachers') in user.groups.all()


def is_student(user):
    return Group.objects.get(name='students') in user.groups.all()

########################################

# Create your views here.


def logout_view(request):
    logout(request)
    return redirect('timetable:login')


@login_required
@user_passes_test(is_teacher)
def teacher_index(request):
    options = {
        ('teacher/register', 'Register new user'),
        ('teacher/groups', 'Add new group for my students'),
        ('teacher/new_participant', 'Add student to group'),
        ('teacher/remove_participant', 'Remove student from group'),
        ('teacher/my_groups', 'List of my groups'),
        ('teacher/communicate', 'Send new communicate'),
        ('teacher/my_profile', 'My personal data'),
    }
    context = {
        'user_type': "TEACHER",
        'options': options,
    }
    return render(request, 'timetable/profile_page.html', context)


@login_required
@user_passes_test(is_student)
def student_index(request):  # TODO
    options = {
        ('student/my_profile', 'My profile'),
        ('student/my_groups', 'My groups'),
        ('student/infos', 'Communicates'),
    }
    context = {
        'user_type': "STUDENT",
        'options': options,
    }
    return render(request, 'timetable/profile_page.html', context)


##############################################

@login_required
@user_passes_test(is_student)
def communicates_list(request):
    student = Student.objects.get(user=request.user)
    set_of_communicates = list()
    for g in student.my_course_groups.all():
        for c in g.communicate_set.all():
            set_of_communicates.append(c)
    set_of_communicates.sort(key=lambda x: x.pub_date, reverse=True)
    context = {
        'header_1': 'COMMUNICATES',
        'info': 'There are all communicates you have received',
        'elements': set_of_communicates
    }
    return render(request, 'timetable/communicates.html', context)


@login_required
@user_passes_test(is_student)
def student_groups(request):
    student = Student.objects.get(user=request.user)
    my_groups = student.my_course_groups
    context = {
        'header_1': 'MY GROUPS',
        'info': '',
        'elements': my_groups
    }
    return render(request, 'timetable/groups.html', context)


##############################


@login_required
def person_profile(request):
    try:
        Student.objects.get(user=request.user)
        student = Student.objects.get(user=request.user)
        context = {
            'user_type': 'student',
            'header_1': 'MY PERSONAL DATA',
            'person': student,
        }
    except Student.DoesNotExist:
            teacher = Teacher.objects.get(user=request.user)
            context = {
                'user_type': 'teacher',
                'header_1': 'MY PERSONAL DATA',
                'person': teacher
            }
    return render(request, 'timetable/info.html', context)


################################
@login_required
@user_passes_test(is_teacher)
def registration(request):
    options = {
        ('register/student/', 'New student'),
        ('register/teacher/', 'New teacher')
    }
    context = {
        'user_type': 'REGISTER',
        'options': options
    }
    return render(request, 'timetable/profile_page.html', context)


@login_required
@user_passes_test(is_teacher)
def new_group(request):
    context = {}
    if request.method == 'POST':
        form = NewGroupForm(request.POST)
        if form.is_valid():
            added_group = form.save(commit=False)
            added_group.teacher = Teacher.objects.get(user=request.user)
            added_group.teacher.subject = Teacher.objects.get(user=request.user).subject
            added_group.save()
            messages.success(request, 'Group successfully added!')
            return redirect('timetable:t_page')
        else:
            return HttpResponse("WHAT ARE YOU DOING?")
    else:
        form = NewGroupForm()
    context = {
        'name': "NEW CREATION",
        'form': form,
        'message': "Create new hour group"
    }
    return render(request, 'timetable/form.html', context)


@login_required
@user_passes_test(is_teacher)
def new_communicate(request):
    context = {}
    if request.method == 'POST':
        form = NewCommunicateForm(request.POST)
        if form.is_valid():
            added_communicate = form.save(commit=False)
            added_communicate.author = Teacher.objects.get(user=request.user)
            added_communicate.save()
            messages.success(request, 'Communicate successfully sent!')
            return redirect('timetable:t_page')
        else:
            return HttpResponse("WHAT ARE YOU DOING?")
    else:
        form = NewCommunicateForm()
    context = {
        'name': "NEW COMMUNICATE",
        'form': form,
        'message': "Create information communicate for your group"
    }
    return render(request, 'timetable/form.html', context)


@login_required
@user_passes_test(is_teacher)
def new_participant(request):
    context = {}
    if request.method == 'POST':
        form = ParticipantForm(request.POST)
        if form.is_valid():
            username = request.POST.get('student_name')
            student = Student.objects.get(user=MyUser.objects.get(username=username))
            hour_group = request.POST.get('group_name')
            chosen_group = HourGroup.objects.get(group_name=hour_group)
            if chosen_group.teacher.user == request.user:
                if chosen_group.participants_amount + 1 <= chosen_group.student_limit:
                    if check_if_possible(student, chosen_group):
                        student.my_course_groups.add(chosen_group)
                        student.save()
                        chosen_group.participants_amount += 1
                        chosen_group.save()
                        messages.success(request, 'Participant successfully added!')
                    else:
                        messages.success(request,
                                         'Student has already something during your course and cannot be added :( ')
                else:
                    messages.success(request, 'Too many participants!')
            else:
                messages.error(request, 'It is not your group! Please ask your colleague!')
            return redirect('timetable:t_page')
        else:
            return HttpResponse("WHAT ARE YOU DOING?")
    else:
        form = ParticipantForm()
    context = {
        'name': "NEW PARTICIPANT",
        'info': "Choose correct group",
        'form': form,
    }
    return render(request, 'timetable/form.html', context)


@login_required
@user_passes_test(is_teacher)
def remove_participant(request):
    context = {}
    if request.method == 'POST':
        form = ParticipantForm(request.POST)
        if form.is_valid():
            username = request.POST.get('student_name')
            student = Student.objects.get(user=MyUser.objects.get(username=username))
            hour_group = request.POST.get('group_name')
            chosen_group = HourGroup.objects.get(group_name=hour_group)
            if chosen_group.teacher.user == request.user:
                if chosen_group in student.my_course_groups.all():
                    student.my_course_groups.remove(chosen_group)
                    student.save()
                    chosen_group.participants_amount -= 1
                    chosen_group.save()
                    messages.success(request, 'Participant successfully removed!')
                else:
                    messages.error(request, 'Not in group!')
            else:
                messages.error(request, 'It is not your group!!')
            return redirect('timetable:t_page')
        else:
            return HttpResponse("WHAT ARE YOU DOING?")
    else:
        form = ParticipantForm()
    context = {
        'name': "REMOVE PARTICIPANT",
        'info': "Choose participant to remove",
        'form': form,
    }
    return render(request, 'timetable/form.html', context)


@login_required
@user_passes_test(is_teacher)
def teacher_groups(request):
    teacher = Teacher.objects.get(user=request.user)
    list_of_groups = HourGroup.objects.filter(teacher=teacher)
    context = {
        'header_1': 'MY GROUPS',
        'elements': list_of_groups,
        'param_1': 'Group names',
        'info': 'Click the group name to see details',
    }
    return render(request, 'timetable/hiperlist.html', context)


@login_required
@user_passes_test(is_teacher)
def group_participants(request, id):
    hour_group = HourGroup.objects.get(id=id)
    list_of_students = set()
    for s in Student.objects.all():
        if hour_group in s.my_course_groups.all():
            list_of_students.add(s.user)
    context = {
        'header_1': 'PARTICIPANTS OF THIS GROUP',
        'elements': list_of_students,
        'param_1': 'First name',
        'param_2': 'Middle name',
        'param_3': 'Last name',
        'group': hour_group,
    }
    return render(request, 'timetable/participants.html', context)


#################################

def home_page(request):
    user = request.user
    teachers = Group.objects.get(name="teachers").user_set.all()
    if user in teachers:
        return redirect('timetable:t_page')
    else:
        return redirect('timetable:s_page')


##########################


class StudentRegisterView(CreateView):
    model = MyUser
    form_class = StudentRegisterForm
    template_name = 'timetable/form.html'

    def get_context_data(self, **kwargs):
        kwargs['info'] = 'Register student'
        kwargs['name'] = 'REGISTER'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        # login(self.request, user)
        messages.success(self.request, 'Student successfully added!')
        return redirect('timetable:t_page')


class TeacherRegisterView(CreateView):
    model = MyUser
    form_class = TeacherRegisterForm
    template_name = 'timetable/form.html'

    def get_context_data(self, **kwargs):
        kwargs['info'] = 'Register teacher'
        kwargs['name'] = 'REGISTER'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        # login(self.request, user)
        messages.success(self.request, 'Teacher successfully added!')
        return redirect('timetable:t_page')


#################################

def check_if_possible(student: Student, hour_group: HourGroup):
    for h in student.my_course_groups.filter(week_day=hour_group.week_day):
        sample_date = datetime(
            2018, 1, 1,
            hour=hour_group.hour.hour, minute=hour_group.hour.minute, second=hour_group.hour.second)
        if hour_group.hour <= h.hour and h.hour <= (sample_date + timedelta(minutes=45)).time():
            return False
    return True
