The aplication to support organisation of extra-curricular activities for students of primary school.

User-Teacher can add new user, create new group for extra classes, add and remove students from own groups, send communicates to students, review own data and informations about own groups participants.

User-Student can review own data, check the list of own groups and read communicates from teachers.

The html files were made using bootstraps from https://www.w3schools.com/bootstrap4/

